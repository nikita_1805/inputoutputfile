FROM openjdk:8
COPY local.yml /opt/dropwizard/
COPY build/libs/InputOutput-1.0-SNAPSHOT-all.jar /opt/dropwizard/
EXPOSE 8080
WORKDIR /opt/dropwizard

ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /opt/dropwizard/InputOutput-1.0-SNAPSHOT-all.jar server local.yml" ]