package files;

import okio.BufferedSink;
import okio.BufferedSource;
import okio.Okio;
import schedulers.quartz.mongo.ScrapIte;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.List;


public class FileIO {
    public static LinkedList<String> readFrom(String fileName) throws IOException {
        File fileIn = new File(fileName);
        LinkedList<String> inputFile = new LinkedList<>();
        BufferedSource reader = Okio.buffer(Okio.source(fileIn));
        {
            while (!reader.exhausted()) {
                inputFile.add(reader.readUtf8());
            }
        }
        reader.close();
        return inputFile;
    }

    public static void writeTo(String path, List<String> jsonInfo) throws IOException {
        File fileOut = new File(path);
        BufferedSink writer = Okio.buffer(Okio.sink(fileOut));
        {
            for (String s : jsonInfo) {
                writer.writeString(s, Charset.defaultCharset());
            }
        }
        writer.close();
    }
    public static void writeToJson(String path, ScrapIte scrapIte) throws IOException {
        File fileOut = new File(path);
        BufferedSink writer = Okio.buffer(Okio.sink(fileOut));
        {

                writer.writeUtf8(String.valueOf(null));

        }
        writer.close();
    }

}
