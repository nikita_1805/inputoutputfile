package files;

import java.io.IOException;
import java.util.LinkedList;

public class Test {
    public static void main(String[] args) throws IOException {
        FileIO fileIO = new FileIO();
        LinkedList<String> fileContent = new LinkedList<>();
        LinkedList<String> outPutContent = new LinkedList<>();

        fileContent = fileIO.readFrom("animals-1.json");
        fileContent.add("\n New elements of JSON file: ");
        System.out.println(fileContent);

        fileIO.writeTo("animals2.json", fileContent);
        fileContent = fileIO.readFrom("animals2.json");
        System.out.println(fileContent);
    }
}
