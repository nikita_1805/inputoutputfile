package jsoup;

import org.jsoup.nodes.Element;

import java.util.LinkedList;
import java.util.List;

public class Item {
    private String title;
    private Element body;
    private String price;
    private String description;
    private LinkedList<String> itemInfo;

    public Item(String title, Element body, String price, String description) {
        this.title = title;
        this.body = body;
        this.price = price;
        this.description = description;
        itemInfo = new LinkedList<>();
    }

    public void itemInfo() {
        this.itemInfo.add(title);
        this.itemInfo.add(description);
        this.itemInfo.add(price);
    }

    public List<String> getItemInfo() {
        return this.itemInfo;
    }

    public String getTitle() {
        return title;
    }

    public Element getBody() {
        return body;
    }

    public String getPrice() {
        return "Price: " + price;
    }

    public String getDescription() {
        return description;
    }
}
