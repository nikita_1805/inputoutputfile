package jsoup;

import files.FileIO;

import java.io.IOException;
import java.util.LinkedList;

public class JsoupTest {
    public static void main(String[] args) throws IOException {
        Parser parser = new Parser("https://www.amazon.com/dp/B07FVYDXBY");
        Item item = new Item(parser.parseTitle(), parser.parseBody(), parser.parsePrice(), parser.parseDescription());
        FileIO fileManager = new FileIO();
        item.itemInfo();
        System.out.println(item.getTitle() + "\n" + item.getDescription() + "\n" + item.getPrice());
        fileManager.writeTo("itemInfo", (LinkedList<String>) item.getItemInfo());


    }
}
