package jsoup;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.List;

public class Parser {
    private org.jsoup.nodes.Document doc;

    public Parser(String url) throws IOException {
        this.doc = init(url);
    }

    private Document init(String url) throws IOException {
        Connection connection = Jsoup.connect(url).userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36");
        try {
            return connection.get();
        } catch (IOException e) {
            System.out.println(e);
            return connection.get();
        }

    }

    public Document getDoc() {
        return doc;
    }

    public void parse(String url) throws IOException {
        this.doc = init(url);
    }

    public String parseTitle() {
        return this.doc.getElementById("productTitle").text();
    }

    public Element parseBody() {
        return this.doc.body();
    }

    public String parsePrice() {
        return this.doc.getElementById("priceblock_ourprice").text();
    }

    public String parseDescription() {
        return this.doc.getElementsByClass("a-unordered-list a-vertical a-spacing-none").text();
    }

    public List<String> elementsList() {
        return this.doc.getElementsByClass("a-size-medium a-color-base a-text-normal").eachText();
    }

    public List<String> itemsIdList() {
        return this.doc.getElementsByAttribute("data-asin").eachText();
    }

    public List<String> itemPriceList() {
        return this.doc.getElementsByClass("a-price").eachText();
    }

    public List<String> itemsLinksList() {
        return this.doc.getElementsByClass("a-size-base a-link-normal a-text-bold").eachText();
    }

    public String parseDetails() {
        return this.doc.getElementById("olp-upd-new-used").text();
    }
}
