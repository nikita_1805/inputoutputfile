package database;

import database.connections.HikaruCP;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import schedulers.quartz.mongo.ScrapIte;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ItemSqlRepository implements Dao {
    private static final String CREATE_ITEM = "insert into itemslist(Price,Name,ProductLink,Id)" + "values(?,?,?,?)";


    public void createTable(String updateQuery) {
        try(Statement state = HikaruCP.getConnection().createStatement()) {
            state.executeUpdate(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void create(ScrapIte item) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()){
            session.beginTransaction();
            session.save(item);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void deleteItem(int id){
        try(Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            ScrapIte item = session.load(ScrapIte.class,id);
            session.delete(item);
            session.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ScrapIte getItem(int id) {
        ScrapIte item = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()){
            session.beginTransaction();
            item = session.get(ScrapIte.class,id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return item;
    }
    public List<ScrapIte> getAll(){
        List<ScrapIte> items = new ArrayList<>();
        String get = "select * from itemslist";
        try (Session session = HibernateUtil.getSessionFactory().openSession()){
            session.beginTransaction();
            NativeQuery getAllQuery = session.createNativeQuery(get);
            items = getAllQuery.list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return items;
    }
    public ScrapIte updateItem(ScrapIte item) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()){
            session.beginTransaction();
            session.update(item);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return getItem((int) item.getId());
    }

}
