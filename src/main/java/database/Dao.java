package database;

import schedulers.quartz.mongo.ScrapIte;

import java.sql.SQLException;

public interface Dao  {
    void create(ScrapIte item);
    void deleteItem(int id);
    ScrapIte getItem(int id);
    ScrapIte updateItem(ScrapIte item);


}
