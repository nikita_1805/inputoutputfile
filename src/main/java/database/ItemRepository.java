package database;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Updates;
import org.bson.Document;
import schedulers.quartz.mongo.ScrapIte;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.set;

public class ItemRepository implements Dao {
    private MongoCollection<Document> itemCollection;
    private final String TITLE = "ItemTitle";
    private final String DESCRIPTION = "ItemDescription";
    private final String PRICE = "ItemPrice";
    private final String DETAILS = "ItemDetails";

    public ItemRepository(MongoDatabase db) {
        this.itemCollection = db.getCollection("itemList");
    }

    public MongoCollection<org.bson.Document> getUserCollection() {
        return itemCollection;
    }

    public void createOne(schedulers.quartz.ScrapItem item) {
        try {
            Document doublecheck = itemCollection.find(eq(item.getTitle())).first();
            if (doublecheck == null) {
                org.bson.Document doc = new org.bson.Document().append(TITLE, item.getTitle())
                        .append(DESCRIPTION, item.getDescription())
                        .append(PRICE, item.getPrice())
                        .append(DETAILS, item.getDetails()).append("Element list", item.getSiteList());
                itemCollection.insertOne(doc);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void create(ScrapIte item) {

        try {

            Document doubleCheck = itemCollection.find(eq("itemName", item.getDataAsin())).first();
            if (doubleCheck == null) {
                Document doc = ScrapIte.toDoc(item);
                itemCollection.insertOne(doc);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteItem(int id) {
        itemCollection.deleteOne(eq("itemID", (long)id));
    }

    public ScrapIte updateItem(ScrapIte item) {
        itemCollection.updateMany(eq("itemID", item.getId()), Updates.combine(set("itemName", item.getDataAsin()),
                set("itemPrice",item.getPrice()),
        set("itemLink",item.getProductLink())));
        return getItem((int)item.getId());
    }

    public ScrapIte getItem(int id) {
        Document doc = itemCollection.find(eq("itemID", (long)id)).first();
        return ScrapIte.from(doc);
    }
}
