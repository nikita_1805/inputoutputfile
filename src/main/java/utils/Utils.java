package utils;

import java.util.List;

public class Utils {
    public static <V> double sum(List<V> in, Mapper<V> mapper) {
        double out = 0.0;
        for (V v : in) {
            out += mapper.map(v);
        }
        return out;
    }

    public interface Mapper<V> {
        double map(V v);
    }


}
