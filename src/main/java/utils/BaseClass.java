package utils;

import java.util.Arrays;
import java.util.List;

public class BaseClass {
    public static void main(String[] args) {
        List<User> list = Arrays.asList(new User(25.0), new User(26.0));
        Double sum = Utils.sum(list, new Utils.Mapper<User>() {
            @Override
            public double map(User user) {
                return user.getAge();
            }
        });
        System.out.println(sum);
    }
}

class User {
    private Double age;

    public User(Double age) {
        this.age = age;
    }

    public Double getAge() {
        return age;
    }
}
