package utils.avgoperator;

import java.util.List;

public class Utils {
    public static <V> double  average(List<V> inList , Mapper<V> map){
        double sum = 0.0;
        double elemNum = 0.0;

        for (V in: inList
             ) {
            sum+=map.avg(in);
            elemNum+=1;
        }
        if(elemNum == 0.0) return 0.0;
        sum/=elemNum;

        return sum;
    }


    public interface Mapper<V>{
         double avg(V v);
    }
}
