package utils.avgoperator;

import java.util.Arrays;
import java.util.List;

public class Base {
    public static void main(String[] args) {
        List<Double> list = Arrays.asList(25.4,26.2,23.3,20.1);
       Double avg = Utils.average(list, new Utils.Mapper<Double>() {
           @Override
           public double avg(Double aDouble) {
               return aDouble;
           }
       });
        System.out.println(avg);
    }

//    private  class User {
//    }
}
