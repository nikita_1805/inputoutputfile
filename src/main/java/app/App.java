package app;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import controllers.ItemController;
import database.Dao;
import database.ItemSqlRepository;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import database.ItemRepository;

import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class App extends Application<AppConfig> {

    public static void main(String[] args) throws Exception {
        new App().run(args);
    }

    @Override
    public void initialize(Bootstrap<AppConfig> bootstrap) {
        super.initialize(bootstrap);
    }

    @Override
    public void run(AppConfig configuration,
                    Environment environment) throws UnknownHostException {

        JacksonJaxbJsonProvider jsonProvider = new JacksonJaxbJsonProvider();
        jsonProvider.setMapper(environment.getObjectMapper());
        environment.jersey().register(jsonProvider);

//        AppConfig.Mongo configurationMongo = configuration.getMongo();
//        MongoClient mongoClient = new MongoManaged(configurationMongo.getUri());
//        MongoDatabase database = mongoClient.getDatabase(configurationMongo.getUri().getDatabase());

//        Dao repository = new ItemRepository(database);
//        final ItemController itemController = new ItemController(repository);
//        environment.jersey().register(itemController);
        Dao repository = new ItemSqlRepository();
        final ItemController itemController = new ItemController(repository);
        environment.jersey().register(itemController);




    }
}
