package app;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mongodb.MongoClientURI;
import io.dropwizard.Configuration;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;

public class AppConfig extends Configuration {
    private Mongo mongo;
    private TlBotConfig botConfig;

    @JsonProperty("swagger")
    public SwaggerBundleConfiguration swaggerBundleConfiguration;

    public Mongo getMongo() {
        return mongo;
    }

    public TlBotConfig getBotConfig() {
        return botConfig;
    }

    public static class Mongo {

        private MongoClientURI uri;

        public MongoClientURI getUri() {
            return uri;
        }
    }

    public static class TlBotConfig{

        private String botUsername;
        private String botToken;

        public String getBotUsername() {
            return botUsername;
        }

        public String getBotToken() {
            return botToken;
        }
    }
}
