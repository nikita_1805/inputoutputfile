package app;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import io.dropwizard.lifecycle.Managed;

import java.net.UnknownHostException;

public class MongoManaged extends MongoClient implements Managed {

    public MongoManaged(MongoClientURI uri) throws UnknownHostException {
        super(uri);
    }

    @Override
    public void start() throws Exception {
    }

    @Override
    public void stop() throws Exception {
        close();
    }
}
