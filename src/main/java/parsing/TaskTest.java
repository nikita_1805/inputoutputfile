package parsing;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;

public class TaskTest {
    public static void main(String[] args)throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String request = "[{\"id\": 1, \"name\": \"test\"},{\"id\": 2, \"name\": \"test1\"},{\"id\": 3, \"name\": \"test2\"}]";

        List<FileExample> parsedList = mapper.readValue(request, new TypeReference<List<FileExample>>() {});

        System.out.println(parsedList);
    }
}
