package parsing;

public class FileExample {
    private Long id;
    private String name;

    public FileExample(Long id, String name){
        this.id = id;
        this.name = name;
    }
    public FileExample(){

    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "FileExample{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
