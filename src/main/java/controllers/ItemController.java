package controllers;

import database.Dao;
import database.ItemRepository;
import schedulers.quartz.mongo.ScrapIte;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/itemsDB")
@Consumes({MediaType.APPLICATION_JSON})
@Produces(MediaType.APPLICATION_JSON)
public class ItemController {
    private Dao repository;

    public ItemController(Dao repository) {
        this.repository = repository;
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Path("/")
    public Response newItem(ScrapIte scrapItem) {
        this.repository.create(scrapItem);
        return Response.ok().build();
    }

    @PUT
    @Path("/")
    public Response updateItemInfo(ScrapIte item){
        ScrapIte scrapItem = this.repository.updateItem(item);
        return Response.ok().entity(scrapItem).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteItem(@PathParam("id") int id) {
        this.repository.deleteItem(id);
        return Response.status(Response.Status.NO_CONTENT).build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/{id}")
    public Response getJSON(@PathParam("id") int id) {
        ScrapIte item = this.repository.getItem(id);
        if (item == null) {
            System.out.println("Get: Item with " + id + " not found");
            return Response.status(204).build();
        }
        return Response.ok().entity(item).build();
    }
}
