package schedulers.quartz;

import files.FileIO;
import jsoup.Parser;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

public class Quartzz {
    public static void main(String[] args) {
        try {
            Parser parser = new Parser("https://www.amazon.com/Thermaltake-Crossfire-Continuous-Certified-PS-SPD-0650NNFABU-1/dp/B07NPM8WZ7/ref=sr_1_2?fst=as%3Aoff&pf_rd_i=16225007011&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=74069509-93ef-4a3c-8dca-a9e3fa773a64&pf_rd_r=H4JDSX6PJATM07VR27KX&pf_rd_s=merchandised-search-4&pf_rd_t=101&qid=1553261093&rnid=16225007011&s=computers-intl-ship&sr=1-2");
            ScrapItem item = new ScrapItem(parser.parseTitle(), parser.parseDescription(), parser.parsePrice(), "new and used", parser.elementsList());
            FileIO fileIO = new FileIO();
            String url = "https://www.amazon.com/Thermaltake-Crossfire-Continuous-Certified-PS-SPD-0650NNFABU-1/dp/B07NPM8WZ7/ref=sr_1_2?fst=as%3Aoff&pf_rd_i=16225007011&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=74069509-93ef-4a3c-8dca-a9e3fa773a64&pf_rd_r=H4JDSX6PJATM07VR27KX&pf_rd_s=merchandised-search-4&pf_rd_t=101&qid=1553261093&rnid=16225007011&s=computers-intl-ship&sr=1-2";
            item.setInfo();

            JobDetail job = JobBuilder.newJob(ScrapJob.class)
                    .withIdentity("ScrapingItemJob", "group1").build();
            Trigger trigger = TriggerBuilder
                    .newTrigger()
                    .withIdentity("ScrapingItemTrigger", "group1")
                    .withSchedule(
                            SimpleScheduleBuilder.simpleSchedule()
                                    .withIntervalInSeconds(10).repeatForever())
                    .build();
            Scheduler scheduler = new StdSchedulerFactory().getScheduler();

            scheduler.getContext().put("item", item);
            scheduler.getContext().put("FileIO", fileIO);
            scheduler.getContext().put("Parser", parser);
            scheduler.getContext().put("URL", url);
            scheduler.start();
            scheduler.scheduleJob(job, trigger);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
