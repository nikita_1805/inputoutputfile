package schedulers.quartz;

import files.FileIO;
import jsoup.Parser;
import org.quartz.*;

import java.io.IOException;
import java.util.Date;

public class ScrapJob implements Job {


    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        Scheduler scheduler = jobExecutionContext.getScheduler();
        Parser parser = null;
        ScrapItem item = null;
        Date date = new Date();
        try {
            parser = (Parser) scheduler.getContext().get("Parser");
            parser.parse((String) scheduler.getContext().get("URL"));
//            item = new ScrapIte(parser.parseTitle(),parser.parseDescription(),parser.parsePrice(),"details");

        } catch (SchedulerException | IOException e) {
            e.printStackTrace();
        }

        if (item == null) {
            item = ScrapItem.EMPTY_SCRAP_ITEM;
        }
        try {
            FileIO.writeTo("QuartzTestScrap.txt", item.getInfo());
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(item.getTitle() + "\n" + item.getPrice() + "\t" + date);

    }
}
