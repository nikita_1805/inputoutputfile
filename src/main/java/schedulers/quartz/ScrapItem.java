package schedulers.quartz;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ScrapItem {
    private String price;
    private String description;
    private String details;
    private String title;
    private List<String> info;
    private List<String> siteList;

    public static final ScrapItem EMPTY_SCRAP_ITEM = new ScrapItem("", "", "", "", new ArrayList<>());


    public ScrapItem(String title, String description, String price, String details, List<String> siteList) {
        this.price = price;
        this.description = description;
        this.details = details;
        this.title = title;
        this.info = new LinkedList<>();
        this.siteList = siteList;
    }

    public void setInfo() {
        this.info.add(title);
        this.info.add(description);
        this.info.add(price);
        this.info.add(details);
    }

    public List<String> getInfo() {
        return info;
    }

    public String getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public String getDetails() {
        return details;
    }

    public String getTitle() {
        return title;
    }

    public List<String> getSiteList() {
        return siteList;
    }
}
