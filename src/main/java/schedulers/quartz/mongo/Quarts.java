package schedulers.quartz.mongo;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import files.FileIO;
import jsoup.Parser;
import database.ItemRepository;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;


public class Quarts {
    public static void main(String[] args) {
        try {
            Parser parser = new Parser("https://www.amazon.com/s?rh=n%3A17923671011&brr=1&rd=1&ref=dp_bc_aui_C_4");
            ScrapIte item = new ScrapIte(222,"1223","78","223");
            FileIO fileIO = new FileIO();
            String url = "https://www.amazon.com/s?rh=n%3A17923671011&brr=1&rd=1&ref=dp_bc_aui_C_4";
            //item.setInfo();
            MongoClient client = new MongoClient("localhost",27017);
            MongoDatabase db = client.getDatabase("itemsBase");
            ItemRepository repository = new ItemRepository(db);

            JobDetail job = JobBuilder.newJob(ScrapNewJob.class)
                    .withIdentity("ScrapingItemJob", "group1").build();
            Trigger trigger = TriggerBuilder
                    .newTrigger()
                    .withIdentity("ScrapingItemTrigger", "group1")
                    .withSchedule(
                            SimpleScheduleBuilder.simpleSchedule()
                                    .withIntervalInSeconds(10).repeatForever())
                    .build();
            Scheduler scheduler = new StdSchedulerFactory().getScheduler();


            scheduler.getContext().put("FileIO", fileIO);
            scheduler.getContext().put("Parser",parser);
            scheduler.getContext().put("URL",url);
            scheduler.getContext().put("Database",repository);
            scheduler.start();
            scheduler.scheduleJob(job, trigger);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
