package schedulers.quartz.mongo;

import org.bson.Document;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.ResultSet;
import java.sql.SQLException;
@Entity
@Table(name = "itemslist")
public class ScrapIte {
    @Column(name = "Price")
    private String price;
    @Column(name = "Name")
    private String dataAsin;
    @Column(name = "ProductLink")
    private String productLink;
    @Id
    @Column(name = "Id")
    private int id;

    public static final ScrapIte EMPTY_SCRAP_ITEM = new ScrapIte(0L, "", "", "");

    public ScrapIte(long id, String price, String dataAsin, String productLink) {
        this.price = price;
        this.dataAsin = dataAsin;
        this.productLink = productLink;
        this.id = (int) id;
    }

    public ScrapIte() {
    }

    public String getPrice() {
        return price;
    }

    public String getDataAsin() {
        return dataAsin;
    }

    public String getProductLink() {
        return productLink;
    }

    public int getId() {
        return id;
    }

    public static ScrapIte from(Document doc) {
        if (doc == null) {
            return null;
        }
        long itemID = (long) doc.getOrDefault("itemID", 0L);
        String itemName = (String) doc.getOrDefault("itemName", "");
        String itemPrice = (String) doc.getOrDefault("itemPrice", "");
        String itemLink = (String) doc.getOrDefault("itemLink", "");

        return new ScrapIte(itemID, itemPrice, itemName, itemLink);
    }

    public static Document toDoc(ScrapIte item) {

        return new Document().append("itemID", item.getId()).
                append("itemName", item.getDataAsin())
                .append("itemPrice", item.getPrice())
                .append("itemLink", item.getProductLink());
    }

    public static ScrapIte fromSql(ResultSet resultSet) throws SQLException {
        if (resultSet == null) {
            return null;
        }
        resultSet.next();
        int itemID = resultSet.getInt("Id");
        String price = resultSet.getString("Price");
        String productLink = resultSet.getString("ProductLink");
        String name = resultSet.getString("Name");

        return new ScrapIte(itemID, price, name, productLink);
    }
    public static ScrapIte allFromSql(ResultSet resultSet) throws SQLException {
        if (resultSet == null) {
            return null;
        }
        int itemID = resultSet.getInt("Id");
        String price = resultSet.getString("Price");
        String productLink = resultSet.getString("ProductLink");
        String name = resultSet.getString("Name");

        return new ScrapIte(itemID, price, name, productLink);
    }

    @Override
    public String toString() {
        return "ScrapIte{" +
                "price='" + price + '\'' +
                ", dataAsin='" + dataAsin + '\'' +
                ", productLink='" + productLink + '\'' +
                ", id=" + id +
                '}';
    }
    public ScrapIte(ScrapIteBuilder builder){
        this.price = builder.price;
        this.dataAsin = builder.dataAsin;
        this.productLink = builder.productLink;
        this.id = (int) builder.id;
    }
    public static class ScrapIteBuilder{
        private String price;
        private String dataAsin;
        private String productLink;
        private long id;

        private ScrapIteBuilder(){}

        public ScrapIteBuilder withPrice(String price){
            this.price = price;
            return this;
        }
        public ScrapIteBuilder withDataAsin(String dataAsin){
            this.dataAsin = dataAsin;
            return this;
        }
        public ScrapIteBuilder withProductLink(String productLink){
            this.productLink = productLink;
            return this;
        }
        public ScrapIteBuilder withId(long id){
            this.id = id;
            return this;
        }
        public ScrapIte build(){
            return new ScrapIte(this);
        }
    }
}
