package schedulers.quartz.mongo;

import jsoup.Parser;
import database.ItemRepository;
import org.quartz.*;

import java.io.IOException;
import java.util.Date;
import java.util.List;

public class ScrapNewJob implements Job {
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        Scheduler scheduler = jobExecutionContext.getScheduler();
        Parser parser = null;
        ScrapIte item = null;
        ItemRepository repository = null;
        Date date = new Date();
        try {
            parser = (Parser) scheduler.getContext().get("Parser");
            parser.parse((String) scheduler.getContext().get("URL"));
            repository = (ItemRepository) scheduler.getContext().get("Database");
        } catch (SchedulerException | IOException e) {
            e.printStackTrace();
        }

        if (repository == null) {
            return;
        }

        List<String> docList = parser.itemsIdList();
        List<String> price = parser.itemPriceList();
        List<String> links = parser.itemsLinksList();
        for (long i = 0; i < price.size(); i++) {
            item = new ScrapIte(i, price.get((int) i), docList.get((int) i), links.get((int) i));
            repository.create(item);
        }


        repository.deleteItem(16);
//       repository.updateItem(12," This device is really good");
//        try {
//            FileIO.writeToJson("DbTestJson.json", repository.getItem(2));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        if (item == null) {
            return;
        }

        System.out.println(item.getPrice() + "\t" + date);


    }
}
