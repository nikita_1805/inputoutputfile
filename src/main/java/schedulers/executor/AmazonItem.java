package schedulers.executor;

import java.util.LinkedList;

public class AmazonItem {
    private String itemInfo;
    private LinkedList<String> info;

    public AmazonItem(String itemInfo) {
        this.itemInfo = itemInfo;
        this.info = new LinkedList<>();
    }

    public String getItemInfo() {
        return itemInfo;
    }

public void setItemInfo(){
        this.info.add(this.itemInfo);
}
    public LinkedList<String> getInfo() {
        return info;
    }
}
