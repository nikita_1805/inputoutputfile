package schedulers.executor.tasks;

import jsoup.Parser;
import schedulers.executor.AmazonItem;

import java.io.IOException;

public class FetchTask implements Runnable {
    private Parser parser;
    private volatile AmazonItem item;

    public FetchTask(Parser parser) throws IOException {
        this.parser = parser;
        this.item = new AmazonItem(parser.parseBody().text());
    }

    @Override
    public void run() {
        FileReadWrite fileReadWrite = new FileReadWrite();
        item.setItemInfo();
        try {
            fileReadWrite.writeTo("AmazonItem.txt", item.getInfo());
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Task one was done");
//        try {
//            TimeUnit.SECONDS.sleep(2);
//        } catch (InterruptedException ex) {
//            throw new IllegalStateException(ex);
//        }
    }
}
