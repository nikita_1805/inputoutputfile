package schedulers.executor.tasks;

import jsoup.Parser;
import schedulers.executor.AmazonItem;

import java.io.IOException;

public class FetchTaskTwo implements Runnable{
    private Parser parser;
    private AmazonItem item;

    public FetchTaskTwo(Parser parser) {
        this.parser = parser;
        this.item = new AmazonItem(parser.parseBody().text());
    }

    @Override
    public void run() {
        FileReadWrite fileReadWrite = new FileReadWrite();
        item.setItemInfo();
        try {
            fileReadWrite.writeTo("AmazonItem2.txt", item.getInfo());
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Task two was done");

//        try {
//            TimeUnit.SECONDS.sleep(2);
//        } catch (InterruptedException ex) {
//            throw new IllegalStateException(ex);
//        }
    }
}
