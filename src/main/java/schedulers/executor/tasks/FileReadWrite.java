package schedulers.executor.tasks;

import okio.BufferedSink;
import okio.BufferedSource;
import okio.Okio;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.LinkedList;

public class FileReadWrite {
    private BufferedSource reader;
    private BufferedSink writer;


    public LinkedList<String> readFrom(String fileName) throws IOException {
        File fileIn = new File(fileName);
        LinkedList<String> inputFile = new LinkedList<>();
        this.reader = Okio.buffer(Okio.source(fileIn));
        {
            while (!this.reader.exhausted()) {
                inputFile.add(this.reader.readUtf8());
            }
        }
        reader.close();
        return inputFile;
    }

    public void writeTo(String path, LinkedList<String> jsonInfo) throws IOException {
        File fileOut = new File(path);
        this.writer = Okio.buffer(Okio.sink(fileOut));
        {
            for (int i = 0; i < jsonInfo.size(); i++) {
                this.writer.writeString(jsonInfo.get(i), Charset.defaultCharset());
            }
        }
        writer.close();
    }


}
