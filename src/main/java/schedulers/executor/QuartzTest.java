package schedulers.executor;

import jsoup.Parser;
import schedulers.executor.tasks.FetchTask;
import schedulers.executor.tasks.FetchTaskTwo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class QuartzTest {
    public static void main(String[] args) throws IOException {
        Parser parser = new Parser("https://www.amazon.com/dp/B07FVYDXBY");

        // System.out.println(parser.parseBody());

        Thread task1 = new Thread(new FetchTask(parser), "FetchTaskOne");
        Thread task2 = new Thread(new FetchTaskTwo(parser), "FetchTaskTwo");
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
        scheduledExecutorService.scheduleAtFixedRate(task1, 10, 5000, TimeUnit.MILLISECONDS);
        scheduledExecutorService.scheduleAtFixedRate(task2, 61, 500, TimeUnit.MILLISECONDS);
        while (true) {

            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String s = br.readLine();
            System.out.println(s);
            if (s.equals("x")) {
                scheduledExecutorService.shutdown();
                break;
            }

        }

    }
}