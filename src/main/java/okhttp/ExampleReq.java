package okhttp;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

public class ExampleReq {
    private OkHttpClient client;

    public ExampleReq() {
        this.client = new OkHttpClient();
    }

    public String getRequest(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();
        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }

    public String getIdRequest(String url, long id) throws IOException {
        Request request = new Request.Builder()
                .url(url + "/" + id)
                .build();
        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }

    public static void main(String[] args) throws IOException {
        ExampleReq client = new ExampleReq();
        String requestInfo = client.getIdRequest("https://api.ukrdisk.com/makers", 693038);
        System.out.println(requestInfo);

    }
}
