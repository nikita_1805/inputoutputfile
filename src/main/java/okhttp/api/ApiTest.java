package okhttp.api;

import okhttp.ExampleReq;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class ApiTest {

    public static void main(String[] args) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        ExampleReq client = new ExampleReq();

        String requestInfo = client.getIdRequest("https://api.ukrdisk.com/makers", 693007);

        ApiUkrDisk apiUkrDisk = mapper.readValue(requestInfo, ApiUkrDisk.class);
        System.out.println(apiUkrDisk);
    }
}
