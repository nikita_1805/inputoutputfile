package okhttp.api;

public class Model {
    private long id;
    private String name;

    public Model(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Model() {

    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "{" + "id:" + this.id + ",name:" + this.name + "}";
    }
}
