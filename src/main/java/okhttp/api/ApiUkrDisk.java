package okhttp.api;

import java.util.List;

public class ApiUkrDisk {

    private Maker maker;
    private List<Model> models;

    public ApiUkrDisk(Maker maker, List<Model> models) {
        this.maker = maker;
        this.models = models;
    }

    public ApiUkrDisk() {

    }

    public Maker getMaker() {
        return maker;
    }

    public List<Model> getModels() {
        return models;
    }

    @Override
    public String toString() {
        return "ApiUkrDisk{" +
                "maker=" + maker +
                ", models=" + models +
                '}';
    }
}
