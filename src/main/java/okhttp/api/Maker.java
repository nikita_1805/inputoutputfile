package okhttp.api;

public class Maker {
    private Long id;
    private String name;

    public Maker(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Maker() {

    }

    public String getName() {
        return name;
    }

    public Long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Maker{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
