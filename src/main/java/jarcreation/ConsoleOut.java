package jarcreation;

import files.FileIO;

import java.io.IOException;
import java.util.LinkedList;

public class ConsoleOut {
    public static void main(String[] args) throws IOException {
        FileIO fileIO = new FileIO();
        LinkedList<String> fileContent = new LinkedList<>();
        fileContent = fileIO.readFrom("animals-1.json");
        System.out.println(fileContent);
    }
}
