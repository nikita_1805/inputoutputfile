package database;

import com.github.fakemongo.Fongo;
import com.mongodb.client.MongoDatabase;

import org.bson.Document;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import schedulers.quartz.mongo.ScrapIte;

import static org.junit.Assert.assertNull;

public class ItemRepositoryTest {
    private ScrapIte item;
    private ScrapIte itemUpdateTest;
    private ItemRepository itemRepository;


    @Before
    public void setUp(){
        item = new ScrapIte(2,"24","easyName","www.dich.com");
        itemUpdateTest = new ScrapIte(2,"24","UPDATED_ASIN","www.dich.com");
        Fongo fongo = new Fongo("mongoServ");
        MongoDatabase db = fongo.getDatabase("myDb");
        itemRepository = new ItemRepository(db);


    }

    @Test
    public void createList() {

        itemRepository.create(item);
        Document doc = new Document()
                .append("itemID",2L).append("itemPrice","24").append("dataAsin","easyName")
                .append("productLink","www.dich.com");

        itemRepository.create(item);


        ScrapIte itemDb = itemRepository.getItem(2);

        Assert.assertEquals(item.getDataAsin(), itemDb.getDataAsin());
        Assert.assertEquals(item.getId(), itemDb.getId());
        Assert.assertEquals(item.getPrice(), itemDb.getPrice());
        Assert.assertEquals(item.getProductLink(), itemDb.getProductLink());
    }

    @Test
    public void deleteItem() {
        itemRepository.create(item);

        itemRepository.deleteItem(2);

        assertNull(itemRepository.getItem(2));
    }

    @Test
    public void updateItem() {
        itemRepository.create(item);

        ScrapIte updatedItemDb = itemRepository.updateItem(itemUpdateTest);

        Assert.assertEquals("UPDATED_ASIN", updatedItemDb.getDataAsin());

    }

    @Test
    public void getItem() {

    }

//    @Test
//    public void getItem() {
//        itemRepository.createList(item);
//        Document doc = itemRepository.getItem(2);
//
//        assertNotNull(itemRepository.getItem(2));
//    }
}